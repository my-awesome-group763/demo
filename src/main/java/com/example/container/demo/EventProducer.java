package com.example.container.demo;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Component;

@Component
public class EventProducer {


  private final StreamBridge streamBridge;

  public EventProducer(StreamBridge streamBridge) {
    this.streamBridge = streamBridge;
  }

  public int produce() {
    streamBridge.send("messageStatusUpdated-out-0", "morango");
    return 2+2;
  }
}

package com.example.container.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
public class SomaTest {

  Soma soma = new Soma();

  @Test
  void test() {
    Assertions.assertEquals(4, soma.soma(2, 2));
  }

}

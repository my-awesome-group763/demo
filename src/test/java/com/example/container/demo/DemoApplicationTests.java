package com.example.container.demo;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singleton;

@AutoConfigureMockMvc
@SpringBootTest
class DemoApplicationTests {

	static KafkaContainer kafka =
					new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"));

	private static Consumer<String, String> kafkaConsumer;

	static {
		kafka.start();

		Map<String, Object> configs =
						new HashMap<>(KafkaTestUtils.consumerProps(kafka.getBootstrapServers(), "test", "true"));
		configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		kafkaConsumer =
						new DefaultKafkaConsumerFactory<>(
										configs, new StringDeserializer(), new StringDeserializer())
										.createConsumer();
		kafkaConsumer.subscribe(singleton("MESSAGE_STATUS_UPDATED"));
	}

	@DynamicPropertySource
	public static void overrideProps(DynamicPropertyRegistry registry) {
		registry.add("spring.cloud.stream.kafka.binder.brokers", () -> kafka.getBootstrapServers());
	}

	@Autowired
	private EventProducer producer;

	@Test
	void givenAValidCommand_whenCallsCreateUserEndpoint_itProduceAnEvent() throws Exception {
		producer.produce();
		ConsumerRecord<String, String> singleRecord =
						KafkaTestUtils.getSingleRecord(
										kafkaConsumer, "MESSAGE_STATUS_UPDATED", 5000);
		Assertions.assertNotNull(singleRecord);
	}

}
